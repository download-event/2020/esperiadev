
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, LayoutAnimation, Keyboard, TouchableWithoutFeedback, Image} from 'react-native';
import * as firebase from "firebase";
import { StatusBar } from 'expo-status-bar';
import API from '../API.js';

export default class LoginScreen extends React.Component {
	static navigationOptions = {
		headerShown: false
	}

	state = {
		email: "",
		password: "",
		token: "",
		errorMessage: null
	};

	/*handleLogin = () => {
		const {email, password} = this.state;
		firebase.auth().signInWithEmailAndPassword(email, password)
		.catch(error => this.setState({errorMessage: error.message}));
	}*/

	handleLogin = () => {
		const data = { email: this.state.email, password: this.state.password };
		fetch("https://hackathon.riccardofacoetti.it/auth/login", {method: "POST", headers: {
			'Content-Type': 'application/json',
			},body: JSON.stringify(data)})
			.then(response => response.json())
			.then((responseJson)=> {
				this.props.navigation.navigate('group');
			})
			.catch(error=>console.log(error)) //to catch the errors if any
	}

	render(){
		LayoutAnimation.easeInEaseOut();
		return (
				<TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
					<SafeAreaView style={styles.container}>
							<StatusBar barStyle="dark" />
							<Image source = {require("../assets/soloMe.png")}style={{ resizeMode:"stretch", width:210, height:150, alignSelf: "center", marginTop: 50, marginBottom: -100 }}/>
							<Text style={styles.greeting}>{" "}</Text>

							<View style={styles.errorMessage}>
								{this.state.errorMessage && <Text style= {styles.error}>{this.state.errorMessage}</Text>}
							</View>

							<View style={styles.form}>
								<TextInput style={styles.input} autoCapitalize="none" onChangeText={email => this.setState({email})} value={this.state.email} placeholder="EMAIL ADDRESS"></TextInput>

								<View style={{marginTop: 32}}>
									<TextInput style={styles.input} secureTextEntry autoCapitalize="none" onChangeText={password => this.setState({password})} value={this.state.password} placeholder="PASSWORD"></TextInput>
								</View>					
							</View>
							
							<TouchableOpacity style={styles.button} onPressOut={this.handleLogin}>
								<Text style={{color: "#FFF", fontWeight: "500"}}>LOG IN</Text>
							</TouchableOpacity>
							<TouchableOpacity style = {{alignSelf: "center", marginTop: 32}} onPress={() => this.props.navigation.navigate("Register")}>
								<Text style= {{color: "#fff", fontSize: 16}}>New to NestIng? <Text style= {{fontWeight: "500", color: "#ffb20f"}}>SIGN UP</Text></Text>
							</TouchableOpacity>
					</SafeAreaView>
				</TouchableWithoutFeedback>

		);
	}
}
const styles = StyleSheet.create({
	container: {
	flex: 1,
	backgroundColor: "#083d77"
     },
	greeting: {
		marginTop: 60,
		fontSize: 30,
		fontWeight: "600",
		textAlign: "center",
		color: "#ffb20f"
	},
	errorMessage:{
		height: 72,
		alignItems: "center",
		justifyContent: "center",
		marginHorizontal: 30
	},
	error: {
		color: "#ffb20f",
		fontSize: 13,
		fontWeight: "600",
		textAlign: "center"
	},
	form: {
		marginBottom: 48,
		marginHorizontal: 30
	},
	inputTitle: {
		color: "#fff",
		fontSize: 12,
		textTransform: "uppercase",
		alignSelf: "center",
		marginBottom: 10
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "#ffb20f",
		borderRadius: 12,
		padding: 5
	},
	button: {
		marginHorizontal: 30,
		backgroundColor: "#ffb20f",
		borderRadius: 8,
		height: 52,
		alignItems: "center",
		justifyContent: "center"
	}
});
