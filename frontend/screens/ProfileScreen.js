import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, SafeAreaView } from "react-native";
import Fire from "../Fire";
import * as firebase from "firebase";
export default class ProfileScreen extends React.Component {

	state = {
		user: this.props.name
	};
/*
	componentDidMount() {
		const user = this.props.uid || Fire.shared.uid;
		this.unsubscribe = Fire.shared.firestore
			.collection("users")
			.doc(user)
			.onSnapshot(doc => {
				this.setState({ user: doc.data() });
			});
	}
	signOutUser = () => {
		firebase.auth().signOut();
	};
	componentWillUnmount() {
		this.unsubscribe();
	}
*/
	render() {
		return (
			<SafeAreaView style={styles.container}>
				<TouchableOpacity style={styles.logout_but}/* onPress={this.signOutUser} */>
					<Text style={{color: "#fff", fontWeight: "500"}}>LOG OUT</Text>
				</TouchableOpacity>
			</SafeAreaView>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: "center",
		alignContent: "center",
		alignSelf: "center"
	},
	profile: {
		marginTop: 64,
		alignItems: "center"
	},
	logout_but: {
		paddingHorizontal: 30,
		position: "relative",
		height: 52,
		borderRadius: 8,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center",
		color: "#fff",
		alignSelf: "center",
	}
});
