import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, LayoutAnimation, FlatList, Image, TextInput, KeyboardAvoidingView, Platform, Switch} from 'react-native';
import {Ionicons} from "@expo/vector-icons"



export default class PollScreen extends React.Component {


	handleSwitch = () => {
		//API
	}

	handleSavePoll = () => {
		//API
	}

	static navigationOptions = {
		headerShown: false
	}
	render(){
		return (
			<View style={styles.container}>
				<View style={{zIndex: 1, marginTop: 10, flexDirection: "row", justifyContent: "space-between"}}>
					<TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
						<Ionicons name="ios-arrow-round-back" size={45} color="#FFF"></Ionicons>
					</TouchableOpacity>
					{/*BACKEND */}
					<TouchableOpacity style={styles.save} onPress={() => this.handleSavePoll}>
						<Text style={{color: "#fff", fontSize: 20}}>Save</Text>
					</TouchableOpacity>
				</View>
				<View style={{alignContent: "center",  flex: 1}}>
					<Text style={{ fontSize: 20, marginTop: 40, marginLeft: 10}}>Write the question</Text>

					<View style={{width: "100%"}}>
						<TextInput style={styles.input}></TextInput>
					</View>
					<Text style={{ fontSize: 20, marginTop: 40, marginLeft: 10}}>Poll options</Text>
					{/*BACKEND */}
					<View style={{width: "100%"}}> 
						<TextInput style={styles.input} placeholder="First option"></TextInput>
						<TextInput style={styles.input} placeholder="Second option"></TextInput>
						<TextInput style={styles.input} placeholder="Third option"></TextInput>
					</View>

					<View style={{flexDirection: "row", justifyContent: "space-between", backgroundColor: "#ffb20f", marginTop: 30, height: 40}}>
						<Text style={{paddingLeft: 10, alignSelf: "center", fontSize: 20}}>
							Multiple choise
						</Text>
						<View style={{alignSelf: "center"}}>
							<Switch style={{marginRight: 10, backgroundColor: "transparent", alignSelf: "center", zIndex: 9}}
								onValueChange = {this.handleSwitch}
								value = {false}
							/>
						</View>
					</View>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	settingList: {
		flexDirection: "column",
		backgroundColor:  "#8A8F9E",
		height: "100%"
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "#ffb20f",

		padding: 5
	},
	header: {
		justifyContent: "space-between",
		flexDirection: "row",
		alignSelf: "center",
		backgroundColor: "#fff",
		height: "12%",
		width: "100%",
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 20
	},
	back: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? 25 : 25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
	save: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? -25 : -25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
	GChatSetting: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? -25 : -25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
});
