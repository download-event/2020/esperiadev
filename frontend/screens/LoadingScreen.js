import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, ActivityIndicator } from 'react-native';
import firebase from "firebase";
import Fire from "../Fire";

export default class LoadingScreen extends React.Component {
	componentDidMount(){
		firebase.auth().onAuthStateChanged(user => {
			this.props.navigation.navigate(user ? "App" : "Auth");
		});
	}
	render(){
		return (
			<SafeAreaView style={styles.container}>
				<Text>loading screen</Text>
				<ActivityIndicator size="large"></ActivityIndicator>
			<StatusBar style="dark" />
			</SafeAreaView>
		);
	}
}
const styles = StyleSheet.create({
	container: {
	flex: 1,
     backgroundColor: '#fff',
     alignItems: 'center',
     justifyContent: 'center',
     },
});
