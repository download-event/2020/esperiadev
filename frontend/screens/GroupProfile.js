import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, LayoutAnimation, FlatList, Image, TextInput, KeyboardAvoidingView, Platform} from 'react-native';
import {Ionicons} from "@expo/vector-icons"

export default class GroupProfile extends React.Component {
	static navigationOptions = {
		headerShown: false
	}
	render(){
		return (
			<View style={styles.container}>
				<View style={{zIndex: 1, marginTop: 10}}>
					<TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
						<Ionicons name="ios-arrow-round-back" size={45} color="#FFF"></Ionicons>
					</TouchableOpacity>
				</View>	
						
				<View>
					<Image source = {require("../assets/tempAvatar.jpg")} style={{ resizeMode:"stretch", width:400, height:400, alignSelf: "center", marginTop: -60, marginBottom: 0 }}/>
				</View>

				<View style={styles.settingList}>
					<TouchableOpacity style={{ height: 50, backgroundColor: "#ffb20f"}} onPress={() => this.props.navigation.navigate("AddMemberScreen")}>
						<View style={{flexDirection: "row", justifyContent: "space-between"}}>
							<Text style={{height: 100, fontSize: 30, marginTop: 9, paddingHorizontal: 10}}>
								Add member
							</Text>
							<Ionicons name="ios-add" size={45} color="black" style={{marginRight: 10}}></Ionicons>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{ height: 50, backgroundColor: "#ffb20f"}}>
						<View style={{flexDirection: "row", justifyContent: "space-between"}}>
							<Text style={{height: 100, fontSize: 30, marginTop: 9, paddingHorizontal: 10}}>
								Manage
							</Text>
							<Ionicons name="ios-color-palette" size={45} color="black" style={{marginRight: 10}}></Ionicons>
						</View>
					</TouchableOpacity>
					<TouchableOpacity style={{ height: 50, backgroundColor: "#ffb20f"}} onPress={() => this.props.navigation.navigate("PollScreen")}>
						<View style={{flexDirection: "row", justifyContent: "space-between"}}>
							<Text style={{height: 100, fontSize: 30,  marginTop: 9, paddingHorizontal: 10}}>
								Create poll
							</Text>
							<Ionicons name="ios-podium" size={45} color="black" style={{marginRight: 10}}></Ionicons>
						</View>
					</TouchableOpacity>
					<View style={{flexDirection: "row", justifyContent: "space-between," , borderTopColor: "black", borderTopWidth: 1}}>
						<TouchableOpacity style={{ height: 150,width: 190 ,backgroundColor: "#ffb20f", justifyContent: "center", alignItems: "center", borderBottomLeftRadius: 30}}>
								<Ionicons name="ios-call" size={55} color="black"></Ionicons>
						</TouchableOpacity>
						<TouchableOpacity style={{ height: 150, width: 190, backgroundColor: "#ffb20f", justifyContent: "center", alignItems: "center", borderBottomRightRadius: 30}}>
								<Ionicons name="ios-videocam" size={70} color="black"></Ionicons>
						</TouchableOpacity>
					</View>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	settingList: {
		flexDirection: "column",
		backgroundColor:  "#8A8F9E",
		height: "100%"
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "#ffb20f",
		borderRadius: 12,
		padding: 5
	},
	header: {
		justifyContent: "space-between",
		flexDirection: "row",
		alignSelf: "center",
		backgroundColor: "#fff",
		height: "12%",
		width: "100%",
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 20
	},
	back: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? 25 : 25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
	GChatSetting: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? -25 : -25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
});
