
import React from 'react';
import { StyleSheet, Text, View, SafeAreaView, TextInput, TouchableOpacity, StatusBar, LayoutAnimation, Platform, Keyboard, TouchableWithoutFeedback, FlatList } from 'react-native';
import {Ionicons} from "@expo/vector-icons"
import * as firebase from "firebase";

export default class RegisterScreen extends React.Component {

	static navigationOptions = {
		headerShown: false
	};
	state = {
		name:"",
		email: "",
		password: "",
		errorMessage: null
	};

	handleSingUp = () => {
		firebase
		.auth()
		.createUserWithEmailAndPassword(this.state.email, this.state.password)
		.then(userCredentials => {
			return userCredentials.user.updateProfile({
				displayName: this.state.name
			});
		})
		.catch(error => this.setState({errorMessage: error.message}));
	};
	render(){
		
		return (

			<TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
			<SafeAreaView style={styles.container}>
				<StatusBar barStyle="dark-content" />
				<View>
					<TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
						<Ionicons name="ios-arrow-round-back" size={45} color="#FFF"></Ionicons>
					</TouchableOpacity>
				</View>
				<Text style={styles.greeting}>{"😉"}</Text>

				<View style={styles.errorMessage}>
					{this.state.errorMessage && <Text style= {styles.error}>{this.state.errorMessage}</Text>}
				</View>

				<View style={styles.form}>
					
					<View>
						<TextInput style={styles.input} autoCapitalize="none" onChangeText={name => this.setState({name})} value={this.state.name} placeholder="USERNAME"></TextInput>
					</View>

					<View style={{marginTop: 32}}>
						<TextInput style={styles.input} autoCapitalize="none" onChangeText={email => this.setState({email})} value={this.state.email} placeholder="EMAIL ADDRESS"></TextInput>
					</View>
					
					<View style={{marginTop: 32}}>
						<TextInput style={styles.input} secureTextEntry autoCapitalize="none" onChangeText={password => this.setState({password})} value={this.state.password} placeholder="PASSWORD"></TextInput>
					</View>					
				</View>
				
				<TouchableOpacity style={styles.button} onPressOut={this.handleSingUp}>
					<Text style={{color: "#FFF", fontWeight: "500"}}>SIGN UP</Text>
				</TouchableOpacity>
				<TouchableOpacity style = {{alignSelf: "center", marginTop: 32}} onPress={() => this.props.navigation.navigate("Login")}>
					<Text style= {{color: "#fff", fontSize: 16}}>Already in NestIng? <Text style= {{fontWeight: "500", color: "#ffb20f"}}>LOG IN</Text></Text>
				</TouchableOpacity>
			</SafeAreaView>
			</TouchableWithoutFeedback>

		);
	}
}
const styles = StyleSheet.create({
	container: {
	flex: 1,
	backgroundColor: "#083d77"
     },
	greeting: {
		position: "absolute",
		marginTop: 60,
		fontSize: 60,
		fontWeight: "400",
		textAlign: "center",
		justifyContent: "center",
		alignItems: "center",
		alignSelf: "center"
	},
	errorMessage:{
		height: 72,
		alignItems: "center",
		justifyContent: "center",
		marginHorizontal: 30
	},
	error: {
		color: "#ffb20f",
		fontSize: 13,
		fontWeight: "600",
		textAlign: "center"
	},
	form: {
		marginBottom: 48,
		marginHorizontal: 30
	},
	inputTitle: {
		color: "#fff",
		fontSize: 10,
		textTransform: "uppercase"
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 15,
		color: "#083d77",
		backgroundColor: "#ffb20f",
		borderRadius: 12,
		padding: 5
	},
	button: {
		zIndex: 1,
		marginHorizontal: 30,
		backgroundColor: "#ffb20f",
		borderRadius: 8,
		height: 52,
		alignItems: "center",
		justifyContent: "center"
	},
	back: {
          top: Platform.OS === 'android' ? 50 : 25,
		left: Platform.OS === 'android' ? 25 : 25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	}
});
