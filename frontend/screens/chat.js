import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, LayoutAnimation, FlatList, Image, TextInput, KeyboardAvoidingView, Platform} from 'react-native';
import {GiftedChat} from "react-native-gifted-chat";
import {Ionicons} from "@expo/vector-icons"
import {groupScreen} from "./groupScreen"
export default class chat extends React.Component {
	static navigationOptions = {
		headerShown: false
	}
	state = {
		messages: "ciao",
	};
	render(){
		const chat =   <GiftedChat></GiftedChat>  /* da API il messaggio deve essere this.state.messages, definisci a quale servizio mandare, definisci mittente*/
		const chatTitle = <Text></Text>
		
		LayoutAnimation.easeInEaseOut();
		if (Platform.OS === "android") {
			<KeyboardAvoidingView style={{flex: 1}} behavior="margin" keyboardVerticalOffset={30} enabled>
				{chat}
			</KeyboardAvoidingView>
		}
		const {navigation} = this.props
		var groupname = navigation.getParam("group.name")
		return (
			<View style={{flex: 1}}>
				<View style={styles.header}>
					<View>
						<TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
							<Ionicons name="ios-arrow-round-back" size={45} color="#FFF"></Ionicons>
						</TouchableOpacity>
					</View>
					<View style={{alignItems: "center", justifyContent: "center", marginTop: 18}}>
						<Text style={{textAlign: "center", fontSize: 20}}>{groupname}</Text>
						{console.log({groupname})}
					</View>
					<View>
						<TouchableOpacity style={styles.GChatSetting} onPress={() => this.props.navigation.navigate("GroupProfile")}>
							<Ionicons name="ios-people" size={45} color="#FFF"></Ionicons>
						</TouchableOpacity>
					</View>
				</View>
				
				{chat}
			
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "#ffb20f",
		borderRadius: 12,
		padding: 5
	},
	header: {
		justifyContent: "space-between",
		flexDirection: "row",
		alignSelf: "center",
		backgroundColor: "#fff",
		height: "12%",
		width: "100%",
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 20
	},
	back: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? 25 : 25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
	GChatSetting: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? -25 : -25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	}
});
