import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, LayoutAnimation, FlatList, Image, TextInput} from 'react-native';
import {Ionicons} from "@expo/vector-icons";
import moment from "moment";
import * as firebase from "firebase";
import API from '../API';

var posts = [
	{
		id: "1",
		name: "Joe McKay",
		text:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage1.jpg")
	},
	{
		id: "2",
		name: "Karyn Kim",
		text:
			"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage2.jpg")
	},
	{
		id: "3",
		name: "Emerson Parsons",
		text:
			"Amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage3.jpg")
	},
	{
		id: "4",
		name: "Kathie Malone",
		text:
			"At varius vel pharetra vel turpis nunc eget lorem. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Adipiscing tristique risus nec feugiat in fermentum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage4.jpg")
	},
	{
		id: "5",
		name: "Joe McKay",
		text:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage1.jpg")
	},
	{
		id: "6",
		name: "Karyn Kim",
		text:
			"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage2.jpg")
	},
	{
		id: "7",
		name: "Emerson Parsons",
		text:
			"Amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage3.jpg")
	},
	{
		id: "8",
		name: "Kathie Malone",
		text:
			"At varius vel pharetra vel turpis nunc eget lorem. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Adipiscing tristique risus nec feugiat in fermentum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage4.jpg")
	},
	{
		id: "9",
		name: "Joe McKay",
		text:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage1.jpg")
	},
	{
		id: "10",
		name: "Karyn Kim",
		text:
			"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage2.jpg")
	},
	{
		id: "11",
		name: "Emerson Parsons",
		text:
			"Amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage3.jpg")
	},
	{
		id: "12",
		name: "Kathie Malone",
		text:
			"At varius vel pharetra vel turpis nunc eget lorem. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Adipiscing tristique risus nec feugiat in fermentum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage4.jpg")
	},
	{
		id: "13",
		name: "Joe McKay",
		text:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage1.jpg")
	},
	{
		id: "14",
		name: "Karyn Kim",
		text:
			"Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage2.jpg")
	},
	{
		id: "15",
		name: "Emerson Parsons",
		text:
			"Amet mattis vulputate enim nulla aliquet porttitor lacus luctus. Vel elit scelerisque mauris pellentesque pulvinar pellentesque habitant.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage3.jpg")
	},
	{
		id: "16",
		name: "Kathie Malone",
		text:
			"At varius vel pharetra vel turpis nunc eget lorem. Lorem mollis aliquam ut porttitor leo a diam sollicitudin tempor. Adipiscing tristique risus nec feugiat in fermentum.",
		timestamp: 1569109273726,
		avatar: require("../assets/tempAvatar.jpg"),
		image: require("../assets/tempImage4.jpg")
	}
];


export default class HomeScreen extends React.Component {
	state = {
		search: '',
	};
	static navigationOptions = {
		title: "Chat"
	}

	updateSearch = (search) => {
		this.setState({ search });
	};

	renderPost = post => {
		return (
			<View style={styles.chatContainer}>
				<Image source={post.avatar} style={styles.avatar} />
				<TouchableOpacity style={{ flex: 1, maxHeight: 30 }}>
					<View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center" }}>
						<View>
							<Text style={styles.name}>{post.name}</Text>
						</View>
					</View>
					<Text style={styles.post}>{post.text}</Text>
				</TouchableOpacity>
			</View>
		);
	};
	render(){
		const { search } = this.state;
		LayoutAnimation.easeInEaseOut();
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.chatTitle}>Chat</Text>
				</View>
				<TextInput style={styles.input} autoCapitalize="none" placeholder="Search"></TextInput>

				<StatusBar style="dark" />

				<FlatList style={styles.feed} data={posts} renderItem={({item}) => this.renderPost(item)} keyExtractor={item => item.id} showsVerticalScrollIndicator={false}>

				</FlatList>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	header: {
		justifyContent: "center",
		alignSelf: "center",
		backgroundColor: "#fff",
		height: "12%",
		width: "100%",
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginLeft: 20,
		marginRight: 20,


	},
	chatTitle: {
		alignSelf: "center",
		fontSize: 20,
		marginTop: 28
	},
	chatContainer: {
		backgroundColor: "#FFF",
		borderRadius: 5,
		padding: 8,
		flexDirection: "row",
		marginVertical: 4,
		marginHorizontal: 4
	},
	chatGo: {
		backgroundColor: "black",
		height: 100,
		width: 100,
		paddingTop: -100
	},
	name: {
		fontSize: 20,
		fontWeight: "500"
	},
	avatar: {
		width: 50,
		height: 50,
		borderRadius: 18,
		marginRight: 16,
		alignSelf: "center"
	},
	input:{
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "transparent",
		padding: 5
	},
});
