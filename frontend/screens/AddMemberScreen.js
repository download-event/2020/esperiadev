import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { SearchBar } from 'react-native-elements';
import { StyleSheet, Text, View, SafeAreaView, TouchableOpacity, LayoutAnimation, FlatList, Image, TextInput, KeyboardAvoidingView, Platform} from 'react-native';
import {Ionicons} from "@expo/vector-icons"

export default class AddMemberScreen extends React.Component {
	static navigationOptions = {
		headerShown: false
	}
	render(){
		LayoutAnimation.easeInEaseOut();
		return (
			<View style={styles.container}>
				<View style={{zIndex: 1, marginTop: 20}}>
					<TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
						<Ionicons name="ios-arrow-round-back" size={45} color="#FFF"></Ionicons>
					</TouchableOpacity>
				</View>	
						
				<View style={{marginTop: 30}}>
					<TextInput style={styles.input} autoCapitalize="none" placeholder="Add member"></TextInput>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	settingList: {
		flexDirection: "column",
		backgroundColor:  "#8A8F9E",
		height: "100%"
	},
	input:{
		borderColor: "#8A8F9E",
		borderBottomWidth: StyleSheet.hairlineWidth,
		height: 40,
		fontSize: 20,
		color: "#083d77",
		backgroundColor: "#ffb20f",
		borderRadius: 12,
		padding: 5
	},
	header: {
		justifyContent: "space-between",
		flexDirection: "row",
		alignSelf: "center",
		backgroundColor: "#fff",
		height: "12%",
		width: "100%",
		borderBottomLeftRadius: 20,
		borderBottomRightRadius: 20,
		marginLeft: 20,
		marginRight: 20,
		paddingTop: 20
	},
	back: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? 25 : 25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
	GChatSetting: {
          top: Platform.OS === 'android' ? 15 : 25,
		left: Platform.OS === 'android' ? -25 : -25,
		width: 45,
		height: 45,
		borderRadius: 16,
		backgroundColor: "#ffb20f",
		alignItems: "center",
		justifyContent: "center"
	},
});
