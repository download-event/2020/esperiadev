import FirebaseKeys from "./config";
import firebase from "firebase"

class Fire {
	constructor() {
		if (!firebase.apps.length) {
			firebase.initializeApp(firebaseKeys);
		}
	}

	addPost = async ({text, localUri}) => {
		const remoteUri = await this.uploadPhotoAsync(localUri)
		return new Promise((res, rej) => {
			this.firestore.collection("posts").add({
				text,
				uid: this.uid,
				timestamp: this.timestamp,
				image:remoteUri
			})
			.then(ref => {
				res(ref)
			})
			.catch(error => {
				rej(error);
			});
		})
	}
	uploadPhotoAsync = async uri => {
		const path = `photos/${this.uid}/${Date.now()}.jpg`
		
		return new Promise(async (res, rej) => {
			const response = await fetch(uri)
			const file = await response.blob()
			let upload = firebase.storage().ref(path).put(file)
			upload.on("state_changed", snapshot => {}, err => {
				rej(err)
			},
			async () => {
				const uri = await upload.snapshot.ref.getDownloadURL();
				res(uri)
			})
		})
	}

	get firestore() {
		return firebase.firestore()
	}

	get uid() {
		return (firebase.auth().currentUser || {}).uid
	}

	get timestamp(){
		return Date.now()
	}
	send = messages => {
		messages.forEach(item => {
			const message = {
				text: item.text,
				timestamp: firebase.database.ServerValue.TIMESTAMP,
				user: item.user
			}

			this.db.push(message)
		})
	}

	parse = message => {
		const {user, text, timestamp} = message.val();
		const {key: _id} = message;
		const createdAt = new Date(timestamp)

		return {
			_id,
			createdAt,
			text,
			user
		}
	}

	get = callback => {
		this.db.on("child_added", snapshot => callback(this.parse(snapshot)));
	};

	off(){
		this.db.off()
	}

	get db() {
		return firebase.database().ref("messages")
	}

	get uid(){
		return (firebase.auth().currentUser || {}).uid
	}
}

Fire.shared = new Fire();
export default Fire;
