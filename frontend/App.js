import React from "react";
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import {createStackNavigator} from "react-navigation-stack";
import {createBottomTabNavigator} from "react-navigation-tabs";
import {Ionicons} from "@expo/vector-icons";
import LoadingScreen from "./screens/LoadingScreen";
import HomeScreen from "./screens/HomeScreen";
import chat from "./screens/chat"
import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import ProfileScreen from "./screens/ProfileScreen";
import groupScreen from "./screens/groupScreen";
import GroupProfile from "./screens/GroupProfile";
import PollScreen from "./screens/PollScreen";
import AddMemberScreen from "./screens/AddMemberScreen";

const AppContainer = createStackNavigator(
  {
    default: createBottomTabNavigator(

    {
      Home: {
        screen: HomeScreen,
        navigationOptions:{
          tabBarIcon: ({tintColor}) => <Ionicons name="ios-chatbubbles" size={30} color={tintColor}/>
        }
      },
      Group: {
        screen: groupScreen,
        navigationOptions:{
          tabBarIcon: ({tintColor}) => <Ionicons name="ios-people" size={30} color={tintColor}/>
        }
      },
      Profile: {
        screen: ProfileScreen,
        navigationOptions:{
          tabBarIcon: ({tintColor}) => <Ionicons name="ios-settings" size={30} color={tintColor}/>
        }
      }
      },
    {
      
      tabBarOptions: {
        activeTintColor: "#ffb20f",
        inactiveTintColor: "black",
        style: {
          backgroundColor: "#fff",
          borderRadius: 20,
          borderColor: "#ffb20f",
          borderTopColor: "transparent"
        }
      }
    },
    ),
  },
    {
      mode: "modal",
      headerMode: "none"
    }
)


const AuthStack = createStackNavigator({
  Login: LoginScreen,
  Register: RegisterScreen,
})


const AppStack = createStackNavigator({
  group: AppContainer, 
  chat: chat,
  GroupProfile: GroupProfile,
  PollScreen: PollScreen,
  AddMemberScreen: AddMemberScreen
},   
  {
    defaultNavigationOptions: {
        header: null
      }
  },
  {
      navigationOptions:{
        header: null,
        headerMode: null
      }
})


export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,
      App: AppContainer,
      Auth: AuthStack,
      AppNavigator: AppStack
    },
    {
      initialRouteName: "Loading"
    }
  )
)