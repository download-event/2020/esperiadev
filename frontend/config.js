import * as firebase from "firebase";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyCvSFhrAHiep6uZPjfbALQsITYOazIgVR8",
  authDomain: "solome-9b976.firebaseapp.com",
  databaseURL: "https://solome-9b976.firebaseio.com",
  projectId: "solome-9b976",
  storageBucket: "solome-9b976.appspot.com",
  messagingSenderId: "547372610956",
  appId: "1:547372610956:web:2eeb82d7db4f6eb60852a9",
  measurementId: "G-NSLVRV2EES"
};
// Initialize Firebase

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export default firebaseConfig