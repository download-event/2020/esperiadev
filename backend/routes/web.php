<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'This is the main page.';
});

$router->group(['prefix' => 'auth'], function () use ($router) {
    $router->group([], function () use ($router) {
        $router->post(
            '/register', ['uses' => 'AuthController@register']
        );

        $router->post(
            '/login', ['uses' => 'AuthController@authenticate']
        );
    });
});

$router->group(['middleware' => 'jwt.auth'], function () use ($router) {
    $router->group(['prefix' => 'users'], function () use ($router) {
        $router->get(
            '/', ['uses' => 'UserController@show']
        );

        $router->put(
            '/', ['uses' => 'UserController@update']
        );
    });

    $router->group(['prefix' => 'groups'], function () use ($router) {
        $router->get('/', ['uses' => 'GroupController@index']);

        $router->post('/', ['uses' => 'GroupController@create']);

        $router->group(['prefix' => '{id}'], function () use ($router) {
            $router->get('/', ['uses' => 'GroupController@show']);

            $router->get('/owner', ['uses' => 'GroupController@showOwner']);

            $router->put('/', ['uses' => 'GroupController@update']);

            $router->delete('/', ['uses' => 'GroupController@delete']);

            $router->post('/addMember/{member_id}', ['uses' => 'GroupController@addMember']);

            $router->group(['prefix' => 'settings'], function () use ($router) {
                $router->get('/', ['uses' => 'GroupSettingsController@show']);

                $router->put('/', ['uses' => 'GroupSettingsController@update']);
            });

            $router->group(['prefix' => 'members'], function () use ($router) {
                $router->get('/', ['uses' => 'GroupController@showMembers']);
            });

            $router->group(['prefix' => 'messages'], function () use ($router) {
                $router->get('/', ['uses' => 'GroupMessageController@index']);

                $router->post('/', ['uses' => 'GroupMessageController@create']);

                $router->delete('/{message_id}', ['uses' => 'GroupMessageController@delete']);
            });

            $router->group(['prefix' => 'questions'], function () use ($router) {
                $router->get('/', ['uses' => 'QuestionController@index']);

                $router->post('/', ['uses' => 'QuestionController@create']);

                //$router->delete('/{message_id}', ['uses' => 'GroupMessageController@delete']);

                $router->group(['prefix' => 'votes'], function () use ($router) {
                    $router->get('/', ['uses' => 'QuestionVoteController@index']);
    
                    $router->post('/', ['uses' => 'QuestionVoteController@create']);
                });

                $router->group(['prefix' => 'options'], function () use ($router) {
                    $router->get('/', ['uses' => 'QuestionOptionController@index']);
    
                    $router->post('/', ['uses' => 'QuestionOptionController@create']);
                });

                $router->group(['prefix' => '{question_id}'], function () use ($router) {
                    $router->put('/', ['uses' => 'QuestionController@update']);

                   /* $router->group(['prefix' => 'votes'], function () use ($router) {
                        $router->post('/', ['uses' => 'QuestionVoteController@create']);
        
                        //$router->delete('/{message_id}', ['uses' => 'GroupMessageController@delete']);
                    });

                    $router->group(['prefix' => 'options'], function () use ($router) {
                        //$router->post('/', ['uses' => 'QuestionOptionController@create']);
        
                        //$router->delete('/{message_id}', ['uses' => 'GroupMessageController@delete']);
                    });*/
                });
            });
        });
    });
});
