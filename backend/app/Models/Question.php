<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $timestamps = true;
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'title', 'description', 'closed', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public function question ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            //->where('group_messages.group_id', $id)
            ->where('questions.closed', false)
            /*->join('users', 'questions.member_id', '=', 'users.id')
            ->join('question_votes', 'question_votes.question_id', '=', 'questions.id')*/
            ->select('questions.*')
            ->get();
    
        return $data;
    }

    public function opened ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            ->where('questions.closed', false)
            ->select('questions.*')
            ->first();
    
        return $data;
    }

    public function votes ($id) {


        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            //->where('group_messages.group_id', $id)
            ->join('users', 'questions.member_id', '=', 'users.id')
            ->join('question_votes', 'question_votes.question_id', '=', 'questions.id')
            ->select('users.username', 'group_messages.id', 'group_messages.message', 'group_messages.created_at')
            ->orderBy('created_at', 'ASC')
            ->get();
    
        return $data;
    }
}