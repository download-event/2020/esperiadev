<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GroupSettings extends Model
{
    public $timestamps = true;
    protected $table = 'group_settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_id', 'name',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}