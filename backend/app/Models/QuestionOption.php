<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class QuestionOption extends Model
{
    public $timestamps = true;
    protected $table = 'question_options';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'option'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function options ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            ->where('questions.closed', false)
            ->join('question_options', 'question_options.question_id', 'questions.id')
            //->where('group_messages.group_id', $id)
            //->join('users', 'questions.member_id', '=', 'users.id')
            //->join('question_votes', 'question_votes.question_id', '=', 'questions.id')
            ->select('question_options.*'/*, 'group_messages.id', 'group_messages.message', 'group_messages.created_at'*/)
            //->orderBy('created_at', 'ASC')
            ->get();
    
        return $data;
    }
}