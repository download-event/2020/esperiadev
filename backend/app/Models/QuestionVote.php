<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class QuestionVote extends Model
{
    public $timestamps = true;
    protected $table = 'question_votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id', 'member_id', 'question_option_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at', 'created_at'
    ];

    public function vote ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            ->where('questions.closed', false)
            ->join('question_votes', 'question_votes.question_id', 'questions.id')
            ->join('question_options', 'question_votes.question_option_id', 'question_options.id')
            ->join('users', 'question_votes.member_id', 'users.id')
            ->select('questions.title', 'users.username', 'question_options.option'/*, 'group_messages.id', 'group_messages.message', 'group_messages.created_at'*/)
            ->get();
    
        return $data;
    }

    public function checkVote ($id, $request) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('questions', 'questions.member_id', '=', 'group_members.user_id')
            ->where('questions.closed', false)
            ->join('question_votes', 'question_votes.question_id', 'questions.id')
            ->select('question_votes.*'/*, 'group_messages.id', 'group_messages.message', 'group_messages.created_at'*/)
            ->get();
    
        return $data;
    }
}