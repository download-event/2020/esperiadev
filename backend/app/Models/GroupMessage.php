<?php

namespace App\Models;

use DB;
use Illuminate\Database\Eloquent\Model;

class GroupMessage extends Model
{
    public $timestamps = true;
    protected $table = 'group_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'member_id', 'message', 'created_at'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    public function group ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.group_id', $id)
            ->join('group_messages', 'group_messages.member_id', '=', 'group_members.user_id')
            //->where('group_messages.group_id', $id)
            ->join('users', 'group_messages.member_id', '=', 'users.id')
            ->select('users.username', 'group_messages.id', 'group_messages.message', 'group_messages.created_at')
            ->orderBy('created_at', 'ASC')
            ->get();
    
        return $data;
    }
}