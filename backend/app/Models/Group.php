<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Group extends Model
{
    public $timestamps = true;
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id', 'settings_id',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function index ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('group_members.user_id', $id)
            ->join('group_settings', 'group_settings.group_id', '=', 'groups.id')
            ->select('groups.id', 'group_settings.name')
            ->get();
    
        return $data;
    }

    public function show ($id, $request) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('groups.id', $id)
            ->where('group_members.user_id', $request->auth->id)
            ->join('group_settings', 'group_settings.group_id', '=', 'groups.id')
            ->select('groups.id', 'group_settings.name')
            ->get();
    
        return $data;
    }

    public function owner ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('groups.id', $id)
            ->join('users', 'group_members.user_id', '=', 'users.id')
            ->select('groups.*', 'users.id', 'users.username')
            ->get();
    
        return $data;
    }

    public function members ($id) {
        $data = DB::table('groups')
            ->join('group_members', 'groups.id', '=', 'group_members.group_id')
            ->where('groups.id', $id)
            ->join('users', 'users.id', '=', 'group_members.user_id')
            ->select('users.id', 'users.username')
            ->get();
    
        return $data;
    }
}