<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\Question;
use App\Models\QuestionOption;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class QuestionOptionController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function index (Request $request, $id) {
        $q = new QuestionOption;

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $question_options = $q->options($id);

        return $question_options ? $question_options : Response()->json([], 404);
    }

    public function create (Request $request, $id) {
        $this->validate($request, [
            'option' => 'required',
        ]);

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $q = new Question;
        $question = $q->opened($id);

        if (!$question) {
            return Response()->json([], 404);
        }

        $question_options = new QuestionOption;
        $question_options->question_id = $question->id;
        $question_options->option = $request->input('option');

        try {
            $question_options->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Question option successfully created.'
        ], 201);
    }

    /*public function delete (Request $request, $id, $message_id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group_message = GroupMessage::find($message_id);

        if (!$group_message) {
            return Response()->json([], 404);
        } if ($group_message->member_id != $request->auth->id) {
            return Response()->json([], 404);
        }

        $group_message->delete();

        return response()->json([
            'success' => 'Message successfully deleted.'
        ], 201);
    }*/
}