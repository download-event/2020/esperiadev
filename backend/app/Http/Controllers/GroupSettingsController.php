<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class GroupSettingsController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function show (Request $request, $id) {
        $settings = GroupSettings::where('group_id', $id)->first();

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        return $settings ? $settings : Response()->json([], 404);
    }

    public function update (Request $request, $id) {
        $settings = GroupSettings::where('group_id', $id)->first();

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        if ($request->input('name')) {
            $settings->name = $request->input('name');
        }

        if ($settings->isDirty()) {
            try {
                $settings->save();
            } catch (Exception $e) {
                return response()->json([
                    'error' => 'An error while updating up.' // To get the error message use this: $e->getMessage()
                ], 400);
            }
        } else {
            return response()->json([
                'error' => 'Nothing to update.'
            ], 400);
        }

        return response()->json([
            'success' => 'Group settings updated successfully.'
        ], 201);
    }
}