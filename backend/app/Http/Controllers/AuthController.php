<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController 
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * Create a new token.
     * 
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60*24 // Expiration time
        ];
        
        // As you can see we are passing `JWT_SECRET` as the second parameter that will 
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    } 

    protected function validateUsername ($username) {
        if (!preg_match('/^[\w]+$/', $username)) {
            return 'Invalid format.';
        }

        return true;
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     * 
     * @param  \App\User   $user 
     * @return mixed
     */
    public function authenticate(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        // Find the user by email
        $user = User::where('email', $this->request->input('email'))->first();

        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the 
            // below respose for now.
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }

        // Bad Request response
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }

    public function register (Request $request) {
        $this->validate($request, [
            'email'     => 'required|email',
            'username'  => 'required',
            'password'  => 'required'
        ]);

        $validate_username = $this->validateUsername($request->input('username'));
        $errors = [];

        if ($validate_username !== true) {
            $errors['username'] = $validate_username;
        }

        if (!empty($errors)) {
            return Response()->json($errors, 400);
        }

        $user = User::where('email', $request->input('email'))->first();

        if ($user) {
            return Response()->json([
                'error' => 'The email is already taken.'
            ], 400);
        } else {
            $user = User::where('username', $request->input('username'))->first();

            if ($user) {
                return Response()->json([
                    'error' => 'The username is already taken.'
                ], 400);
            }
        }

        $hashed_password = Hash::make($this->request->input('password'));

        $user = new User;
        $user->username = $request->input('username');
        $user->email = $request->input('email');
        $user->password = $hashed_password;

        try {
            $user->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Successfully registered.',
            'token' => $this->jwt($user)
        ], 201);
    }

    /**
     * Refresh the user token if middleware validate the provided refresh token.
     *
     * @param \App\User $user
     * @return mixed
     */
    public function refreshToken(Request $request) {
        $user = $request->auth;

        return response()->json([
            'access_token' => $this->jwt($user, 60 * 60)
        ], 200);
    }
}