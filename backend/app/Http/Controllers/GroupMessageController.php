<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupMessage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class GroupMessageController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function index (Request $request, $id) {
        $m = new GroupMessage;

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $messages = $m->group($id);

        return $messages ? $messages : Response()->json([], 404);
    }

    public function create (Request $request, $id) {
        $this->validate($request, [
            'message' => 'required',
        ]);

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group_message = new GroupMessage;
        $group_message->member_id = $request->auth->id;
        $group_message->message = $request->input('message');

        try {
            $group_message->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Message successfully sent.'
        ], 201);
    }

    public function delete (Request $request, $id, $message_id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group_message = GroupMessage::find($message_id);

        if (!$group_message) {
            return Response()->json([], 404);
        } if ($group_message->member_id != $request->auth->id) {
            return Response()->json([], 404);
        }

        $group_message->delete();

        return response()->json([
            'success' => 'Message successfully deleted.'
        ], 201);
    }
}