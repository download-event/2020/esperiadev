<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\Question;
use App\Models\QuestionVote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class QuestionVoteController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function index (Request $request, $id) {
        $q = new QuestionVote;

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $question_votes = $q->vote($id);

        return $question_votes ? $question_votes : Response()->json([], 404);
    }

    public function create (Request $request, $id) {
        $this->validate($request, [
            'option_id' => 'required',
        ]);

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $q = new Question;
        $question = $q->opened($id);

        if (!$question) {
            return Response()->json([], 404);
        }

        $qv = new QuestionVote;
        $question_votes = $qv->checkVote($id, $request);

        $hasVote = false;
        foreach ($question_votes as $vote) {
            if ($vote->member_id == $request->auth->id) {
                $hasVote = true;
            }
        }

        if ($hasVote) {
            return Response()->json([
                'error' => 'Vote already sent.'
            ], 400);
        }

        $question_vote = new QuestionVote;
        $question_vote->question_id = $question->id;
        $question_vote->member_id = $request->auth->id;
        $question_vote->question_option_id = $request->input('option_id');

        try {
            $question_vote->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Question vote successfully sent.'
        ], 201);
    }

    /*public function delete (Request $request, $id, $message_id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group_message = GroupMessage::find($message_id);

        if (!$group_message) {
            return Response()->json([], 404);
        } if ($group_message->member_id != $request->auth->id) {
            return Response()->json([], 404);
        }

        $group_message->delete();

        return response()->json([
            'success' => 'Message successfully deleted.'
        ], 201);
    }*/
}