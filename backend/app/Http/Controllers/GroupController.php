<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\GroupSettings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class GroupController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function index (Request $request) {
        $g = new Group;
        $groups = $g->index($request->auth->id);

        return $groups ? $groups : Response()->json([], 404);
    }

    public function show (Request $request, $id) {
        $g = Group::find($id);

        if (!$g) {
            return Response()->json([], 404);
        } if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group = $g->show($id, $request);

        return $group ? $group : Response()->json([], 404);
    }

    public function showOwner (Request $request, $id) {
        $g = Group::find($id);
        $group_member = GroupMember::where('group_id', $id)->first();

        if (!$g) {
            return Response()->json([], 404);
        } if ($group_member->user_id != $request->auth->id) {
            return Response()->json([], 404);
        }

        $group = $g->owner($id);

        return $group ? $group : Response()->json([], 404);
    }

    public function create (Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $group = new Group;
        $group->owner_id = $request->auth->id;
        $group->settings_id = 0;

        try {
            $group->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        $group_member = new GroupMember;
        $group_member->group_id = $group->id;
        $group_member->user_id = $request->auth->id;

        try {
            $group_member->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        $group_settings = new GroupSettings;
        $group_settings->group_id = $group->id;
        $group_settings->name = $request->input('name');

        try {
            $group_settings->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        $group->settings_id = $group_settings->id;

        try {
            $group->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Group successfully created.'
        ], 201);
    }

    public function update (Request $request, $id) {
        $group = Group::find($id);
        $group_member = GroupMember::where('group_id', $id)->first();
        $group_members = GroupMember::where('user_id', $request->auth->id)->get();

        if (!$group || !$group_member || !$group_members) {
            return Response()->json([], 404);
        }

        // Check if user has access to the group
        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) { 
            return Response()->json([], 404);
        }

        if ($group->isDirty()) {
            try {
                $group->save();
            } catch (Exception $e) {
                return response()->json([
                    'error' => 'An error while updating up.' // To get the error message use this: $e->getMessage()
                ], 400);
            }
        } else {
            return response()->json([
                'error' => 'Nothing to update.'
            ], 400);
        }

        return response()->json([
            'success' => 'Group updated successfully.'
        ], 201);
    }

    public function delete (Request $request, $id) {
        $group = Group::find($id);
        $group_member = GroupMember::where('group_id', $id)->first();
        $group_members = GroupMember::where('user_id', $request->auth->id)->get();

        if (!$group || !$group_member || !$group_members) {
            return Response()->json([], 404);
        }

        // Check if user has access to the group
        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) { 
            return Response()->json([], 404);
        }

        $group_settings = GroupSettings::where('group_id', $id)->first();

        $group->delete();
        $group_member->delete();
        $group_settings->delete();

        return response()->json([
            'success' => 'Group deleted successfully.'
        ], 201);
    }

    public function addMember (Request $request, $id, $member_id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }
        
        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $member_id) {
                $hasGroup = true;
            }
        }

        if ($hasGroup) {
            return Response()->json([
                'error' => 'User is already in the group.'
            ], 400);
        }

        $group_member = new GroupMember;
        $group_member->group_id = $id;
        $group_member->user_id = $member_id;

        try {
            $group_member->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Member group successfully created.'
        ], 201);
    }

    public function showMembers (Request $request, $id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }
        $g = new Group;
        $group_members = $g->members($id);
        
        return $group_members ? $group_members : Response()->json([], 404);
    }
}