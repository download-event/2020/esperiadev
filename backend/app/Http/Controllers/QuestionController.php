<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Group;
use App\Models\GroupMember;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class QuestionController extends Controller
{
    public function checkMember ($id, $request) {
        $group = Group::find($id);

        if (!$group) {
            return false;
        }

        $group_members = GroupMember::where('group_id', $id)->get();

        $hasGroup = false;
        foreach ($group_members as $member) {
            if ($member->user_id == $request->auth->id) {
                $hasGroup = true;
            }
        }

        if (!$hasGroup) {
            return false;
        }

        return true;
    }

    public function index (Request $request, $id) {
        $q = new Question;

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $question = $q->question($id);

        return $question ? $question : Response()->json([], 404);
    }

    public function create (Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $q = new Question;
        $questions = $q->opened($id);

        if ($questions) {
            return Response()->json([
                'error' => 'A question is already opened.'
            ], 400);
        }

        $question = new Question;
        $question->member_id = $request->auth->id;
        $question->title = $request->input('title');
        $question->description = $request->input('description');
        $question->closed = false;

        try {
            $question->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Question successfully created.'
        ], 201);
    }

    public function update (Request $request, $id, $question_id) {
        $this->validate($request, [
            'closed' => 'required'
        ]);

        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        if ($request->input('closed') != 1) {
            return Response()->json([
                'Closed' => 'The value can be only true (1).'
            ], 400);
        }

        $question = Question::find($question_id);

        if ($question->closed == true) {
            return Response()->json([
                'error' => 'The question must be opened.'
            ], 400);
        }

        $question->closed = $request->input('closed');

        try {
            $question->save();
        } catch (Exception $e) {
            return response()->json([
                'error' => 'An error while signing up.' // To get the error message use this: $e->getMessage()
            ], 400);
        }

        return response()->json([
            'success' => 'Question successfully updated.'
        ], 201);
    }

    /*public function delete (Request $request, $id, $message_id) {
        if (!$this->checkMember($id, $request)) {
            return Response()->json([], 404);
        }

        $group_message = GroupMessage::find($message_id);

        if (!$group_message) {
            return Response()->json([], 404);
        } if ($group_message->member_id != $request->auth->id) {
            return Response()->json([], 404);
        }

        $group_message->delete();

        return response()->json([
            'success' => 'Message successfully deleted.'
        ], 201);
    }*/
}