# Download Hackathon 2020 team EsperiaDev

The project is divided into two parts:
- backend, made in php;
- frontend, made in javascript, using react native.

There is also a Database directory which contains the E/R scheme.

## Backend

To create the backend we used the Lumen micro-framework (by Laravel) and we created
APIs that allow our application to integrate with other services, thus reaching more people!

The code can either run locally, or use the bees available directly at:
hackathon.riccardofacoetti.it.

To run locally the backend, follow this instructions:
- download the 'backend' directory.
	- IF you don't have [composer](https://getcomposer.org/download/) (php dependency manager), download and install it.
- Open terminal
- move in 'backend' directory 
- type
```composer
composer install
```
- type 'php -S localhost:8000 -t public'.
Now you can call the api using the 'localhost:8000' route.

You can find the APIs at the end of this file!

## Frontend

To create the backend we used a Javascript framework called React Native, that allowed us to create
both Android and iOS frontend application.

To run locally the frontend, follow this instructions:
- download the 'frontend' directory.
	- IF you don't have [npm](https://www.npmjs.com/get-npm), download and install it.
- Open terminal
- move in 'frontend' directory 
- type
```expo
npm install -g expo-cli
```
- type
```npm
npm start
```

## API documentation:

ERRORS

	The application will respond with these errors:
		- 401, unauthorized, the API requires the token to authenticate
		- 404, page not found
		- 400, system error, maybe for bad parameters provided, anyway it can be visible on response "error"

AUTH
	login
		/auth/login		POST
		
		required: 
			- email 		:string
			- password 		:string
			
		response:
			- token
			
	register
		/auth/register	POST
		
		required: 
			- username		:string
			- email			:string
			- password		:string
			
		response:
			- token
			
USERS
	
	user info
		/users			GET
			
			required: 
				- token		:string
				
			response
				- user's info
			
	update user
		/users			PUT
			
			required:
				- token		:string
			
			optional: 
				- username		:string
				- email			:string
				- password		:string
				
			response:
				- string
			
GROUPS

	show all user's group
		/groups			GET
		
			required:
				- token		:string
				
			response:
				- user's groups
		
	create group
		/groups			POST
		
			required:
				- name		:string
				- token		:string
				
			response:
				- string
				
	show group
		/groups/{id}	GET
		
			required:
				- token		:string
				
			response:
				- group
				
	delete group
		/groups/{id}	DELETE
		
			required:
				- token		:string
				
			response:
				- string
				
	add member on group
		/groups/{id}/addMember/{member_id}		POST
		
			required:
				- token		:string
				
			response:
				- string
				
	show group' settings
		/groups/{id}/settings		GET
		
			required:
				- token		:string
				
			response:
				- group' settings
				
	update group' settings
		/groups/{id}/settings		UPDATE
		
			required:
				- token		:string
				
			optional: 
				- name		:string
				
			response:
				- string
			
	show group's members
		/groups/{id}/members		GET
		
			required:
				- token		:string
				
			response:
				- group's members
				
	show group's messages
		/groups/{id}/messages		GET
		
			required:
				- token		:string
				
			response:
				- group's messages
				
	create group's message
		/groups/{id}/messages		POST
		
			required:
				- token		:string
				
			response:
				- string
	
	delete group's message
		/groups/{id}/messages/{message_id}		DELETE
		
			required:
				- token		:string
				
			response:
				- string
				
	show group's questions
		/groups/{id}/questions		GET
		
			required:
				- token		:string
				
			response:
				- group's questions
				
	create group's question
		/groups/{id}/questions		POST
		
			required:
				- message		:string
				- token			:string
				
			response:
				- string
				
	update group's question
		/groups/{id}/questions/{question_id}		PUT
		
			required:
				- token		:string
				
			optional:
				- closed 	:boolean
				
			response:
				- group
				
	show group's question votes
		/groups/{id}/questions/votes		GET
		
			required:
				- token		:string
					
			response:
				- group's question votes
				
	create group's question vote
		/groups/{id}/questions/votes		POST
		
			required:
				- option_id		:unsigned int
				- token			:string
				
			response:
				- string
				
	show group's question options
		/groups/{id}/questions/options		GET
		
			required:
				- token		:string
				
			response:
				- group's question option_id
				
	create group's question option
		/groups/{id}/questions/options		POST
		
			required:
				- option		:string
				- token			:string
				
			response:
				- string
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			
